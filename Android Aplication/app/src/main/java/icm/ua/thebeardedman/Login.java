package icm.ua.thebeardedman;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.VideoView;

/**
 * Created by tiagoteixeira on 19-11-2015.
 */
public class Login extends Activity {

    public final static String EXTRA_MESSAGE = ".Login";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        VideoView myVideoView = (VideoView) findViewById(R.id.videoView);

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("The Bearded Man");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.beard));
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        myVideoView.requestFocus();
        progressDialog.dismiss();
        myVideoView.start();
    }

    public void login (View v){
        Intent intent = new Intent(Login.this, Menu.class);
        intent.putExtra(EXTRA_MESSAGE, R.layout.content_main);
        startActivity(intent);
        this.finish();
    }

    public void fazerLogin (View v){
        Button button = (Button) findViewById(R.id.button2);
        button.setVisibility(View.GONE);
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linear);
        linearLayout.setVisibility(View.VISIBLE);
    }

    public void clearText (View v){
        EditText text;
        if(v.getId() == R.id.textUser) {
            text = (EditText) findViewById(R.id.textUser);
        } else {
            text = (EditText) findViewById(R.id.textPass);
        }
        text.setText("");
    }
}
