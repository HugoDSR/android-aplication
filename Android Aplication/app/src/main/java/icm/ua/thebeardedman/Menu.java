package icm.ua.thebeardedman;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Menu extends NavigationDrawer {

    public final static String EXTRA_MESSAGE = ".Menu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        TextView tv = (TextView) findViewById(R.id.textView5);

        tv.setText(pecas.size() + " " + getString(R.string.produtosLista));

        LinearLayout linearLayoutPai = (LinearLayout) findViewById(R.id.linearlayout);



        String[] nomes = {"T-Shirt Beard Man", "Blusão Oxford", "Camisola LumberSexual", "Jaqueta Beard","Sweatshirt Style"};

        for (int i = 0; i < links.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT));

            ImageView imageView = new ImageView(this);
            final float scale = getResources().getDisplayMetrics().density;
            imageView.setLayoutParams(new LayoutParams((int) (250 * scale), (int) (250 * scale)));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            new DownloadImageTask(imageView).execute(links.get(i));

            TextView textView = new TextView(this);
            textView.setText(nomes[i]);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            textView.setLayoutParams(params);
            textView.setTextColor(Color.BLACK);

            linearLayout.addView(imageView);
            linearLayout.addView(textView);

            linearLayoutPai.addView(linearLayout);
        }

    }

    public void registo(View v) {
        Intent intent = new Intent(this, Scanner.class);
        intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.scan);
        startActivity(intent);
        finish();
    }

    public void lista(View v) {
        Intent intent = new Intent(this, MyList.class);
        intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.shoplist);
        startActivity(intent);
        finish();
    }

}
